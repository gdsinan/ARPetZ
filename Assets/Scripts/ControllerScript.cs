using System;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ControllerScript : MonoBehaviour
{
    public GameObject placementIndicator;
    public ARRaycastManager arRaycastManager;
    public GameObject[] animalsToSpawn;


    [SerializeField] private GameObject foxPrefab;
    [SerializeField] private GameObject wolfPrefab;
    [SerializeField] private GameObject alpacaPrefab;
    [SerializeField] private Camera cam;


    private GameObject spawnedObject;
    private Pose PlacementPose;
    private ARRaycastManager aRRaycastManager;
    private bool placementPoseIsValid = false;




    void Start()
    {
        // used to test that annoying error message
        if (arRaycastManager == null)
        {
            Debug.LogError("ARRaycastManager is not assigned.");
        }
    }


    void Update()
    {
        UpdatePlacementIndicator();
        UpdatePlacementPose();


        // test spawner - not working
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log($"Space key was pressed");
            SpawnRandomAnimal();
        }
    }

    // manages the placement indicator
    void UpdatePlacementIndicator()
    {
        if (spawnedObject == null && placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(PlacementPose.position, PlacementPose.rotation);
        }

        else
        {
            placementIndicator.SetActive(false);
        }
    }

    // manages the placement pose of pet
    void UpdatePlacementPose()
    {
        var screenCenter = cam.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        arRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);


        placementPoseIsValid = hits.Count > 0;
        if(placementPoseIsValid && spawnedObject == null)
        {
            PlacementPose = hits[0].pose;
            Debug.Log($"TEST");
        }
    }


    // Instantiate the pets - not working
    //void ARPlaceObject()
    //{
    //    spawnedObject = Instantiate(arObjectToSpawn, PlacementPose.position, PlacementPose.rotation);
    //}


    // Spawn a random animal
    void SpawnRandomAnimal()
    {
        int randIndex = Random.Range(0, animalsToSpawn.Length);

        if (animalsToSpawn[randIndex] != null && placementPoseIsValid)
        {
            spawnedObject = Instantiate(animalsToSpawn[randIndex], PlacementPose.position, PlacementPose.rotation);
        }
    }
}
