using UnityEngine;

public class AlpacaController : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // method to test animations
    void AnimTester()
    {
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            animator.Play("aidle");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            animator.Play("aidle2");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            animator.Play("aattack");
        }
    }


    void Update()
    {
        AnimTester();
    }
}
