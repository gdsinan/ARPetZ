using UnityEngine;

public class FoxController : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }
    
    // method to test animations
    void AnimTester()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            animator.Play("idle");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            animator.Play("idle2");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            animator.Play("attack");
        }
    }


    void Update()
    {
        AnimTester();
    }
}
