using UnityEngine;

public class WolfController : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // method to test animations
    void AnimTester()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            animator.Play("widle");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            animator.Play("widle2");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            animator.Play("wattack");
        }
    }


    void Update()
    {
        AnimTester();
    }
}
